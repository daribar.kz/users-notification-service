package cache

type Cache struct {
	OrderCache *OrderCache
}

func NewCache() *Cache {
	return &Cache{OrderCache: NewOrderCache()}
}
