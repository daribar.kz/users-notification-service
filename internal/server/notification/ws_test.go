package notification

import (
	"daribar/users-notification-service/pkg/logger"
	"github.com/gorilla/websocket"
	"net/http"
	"testing"
	"time"
)

func init() {
	logger.SetLogger(logger.NewZapLogger())
}

func TestHub(t *testing.T) {
	var hub = NewHub()
	go hub.Run()

	var conn *websocket.Conn
	var clients = []*Client{
		{
			hub:   hub,
			conn:  conn,
			Send:  make(chan *Message),
			Phone: "111",
		},
		{
			hub:   hub,
			conn:  conn,
			Send:  make(chan *Message),
			Phone: "222",
		},
	}

	time.Sleep(4 * time.Second)
	for _, client := range clients {
		hub.Register <- client
	}

	time.Sleep(2 * time.Second)

	for _, client := range clients {
		if ok := hub.Clients[client]; !ok {
			t.Fail()
		}

		hub.Unregister <- client
	}

	time.Sleep(2 * time.Second)

	for _, client := range clients {
		if ok := hub.Clients[client]; ok {
			t.Fail()
		}
	}
}

func echo(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}
	defer c.Close()
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			break
		}
		err = c.WriteMessage(mt, message)
		if err != nil {
			break
		}
	}
}
