package handlers

import (
	cache2 "daribar/users-notification-service/internal/cache"
	"daribar/users-notification-service/internal/listeners"
	"daribar/users-notification-service/internal/server/notification"
	"daribar/users-notification-service/pkg/domain"
	"daribar/users-notification-service/pkg/logger"
	"encoding/json"
	"fmt"
	"github.com/nats-io/nats.go"
	"net/http"
	"strconv"
	"time"
)

type Handler struct {
	cache *cache2.Cache
	nc    *nats.Conn
	hub   *notification.Hub
}

func NewHandler(hub *notification.Hub, cache *cache2.Cache, nc *nats.Conn) *Handler {
	return &Handler{hub: hub, cache: cache, nc: nc}
}

func (h *Handler) WSHandler(w http.ResponseWriter, r *http.Request) {
	notification.ServeWs(h.hub, w, r)
}

func (h *Handler) WSTestHandler(w http.ResponseWriter, r *http.Request) {
	var phone = r.FormValue("phone")
	var idStr = r.FormValue("id")
	var isOpen = r.FormValue("isOpen")
	var id, _ = strconv.Atoi(idStr)

	rcvOrder := listeners.ReceivedOrder{
		ID: int64(id),
	}

	h.cache.OrderCache.SetOrder(domain.Order{
		ID:        int64(id),
		Phone:     phone,
		CreatedAt: time.Now(),
		IsOpen:    isOpen == "open",
	})

	b, err := json.Marshal(rcvOrder)
	if err != nil {
		fmt.Println(err)
	}

	subj, msg := "order_"+isOpen, b
	if err = h.nc.Publish(subj, msg); err != nil {
		logger.Error(err)
	}
}
