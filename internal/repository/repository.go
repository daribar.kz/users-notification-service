package repository

import (
	"daribar/users-notification-service/internal/repository/postgres"
	"database/sql"
)

type Repository struct {
	postgres.Postgres
}

func NewRepository(postgresConn *sql.DB) *Repository {
	return &Repository{Postgres: postgres.NewRepository(postgresConn)}
}
