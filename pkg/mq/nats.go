package mq

import (
	"daribar/users-notification-service/internal/config"
	"daribar/users-notification-service/pkg/logger"
	"fmt"
	"github.com/nats-io/nats.go"
)

func NewNats(cfg *config.Config) (*nats.Conn, error) {
	opts := nats.GetDefaultOptions()
	opts.Name = "NATS Sample Subscriber"
	opts.Url = cfg.NatsAddress

	opts.ReconnectedCB = func(nc *nats.Conn) {
		logger.Warn(fmt.Sprintf("NATS reconnected, url: %v", nc.ConnectedUrl()))
	}
	opts.ClosedCB = func(nc *nats.Conn) {
		logger.Warn("NATS closed")
	}

	nc, err := opts.Connect()
	return nc, err
}
