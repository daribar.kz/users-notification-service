package main

import (
	"context"
	cache2 "daribar/users-notification-service/internal/cache"
	"daribar/users-notification-service/internal/config"
	listeners2 "daribar/users-notification-service/internal/listeners"
	"daribar/users-notification-service/internal/repository"
	"daribar/users-notification-service/internal/server"
	"daribar/users-notification-service/internal/server/notification"
	service2 "daribar/users-notification-service/internal/service"
	"daribar/users-notification-service/pkg/database"
	"daribar/users-notification-service/pkg/logger"
	"daribar/users-notification-service/pkg/mq"
	"database/sql"
	"fmt"
	"github.com/redis/go-redis/v9"
	"os"
	"os/signal"
	"syscall"
)

// init logger
func init() {
	logger.SetLogger(logger.NewZapLogger())
}

func main() {
	cfg, err := config.New()
	if err != nil {
		logger.Fatal(fmt.Sprintf("failed to initialize config, err: %v", err))
	}

	p, err := database.NewPostgres(cfg)
	if err != nil {
		logger.Fatal(err)
	}
	defer func(pr *sql.DB) {
		if err = pr.Close(); err != nil {
			logger.Fatal(err)
		}
	}(p)

	ch := database.NewRedis()
	defer func(cdb *redis.Client) {
		if err = cdb.Close(); err != nil {
			logger.Fatal(err)
		}
	}(ch)

	repository.NewRepository(p)
	var nc, _ = mq.NewNats(cfg)
	var cache = cache2.NewCache()
	var hub = notification.NewHub()
	var orderService = service2.NewOrderService(hub, cache)
	var service = service2.NewService(orderService)
	var listeners = listeners2.NewListener(service, cache, nc)

	go hub.Run()
	go listeners.ListenOrderClose(context.Background())
	go listeners.ListenOrderOpen(context.Background())
	go server.Listen(hub, nc, cache)

	logger.Info("users_notification_service started")

	var quit = make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	var osSignal = <-quit

	logger.Info(fmt.Sprintf("program shutdown... call_type: %v", osSignal))
}
