package cache

import (
	"daribar/users-notification-service/pkg/domain"
	"testing"
)

func TestSetOrderCache(t *testing.T) {
	var oCache = NewOrderCache()
	var order = domain.Order{
		ID:     2,
		Phone:  "111",
		IsOpen: true,
	}
	oCache.SetOrder(order)

	if oCache.GetOrder(2) != order {
		t.Fail()
	}
}
