package service

type Service struct {
	Order *OrderService
}

func NewService(order *OrderService) *Service {
	return &Service{Order: order}
}
