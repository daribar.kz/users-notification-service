package notification

import (
	"daribar/users-notification-service/pkg/logger"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	Clients map[*Client]bool

	// Inbound messages from the clients.
	Broadcast chan *Message

	// Register requests from the clients.
	Register chan *Client

	// Unregister requests from clients.
	Unregister chan *Client
}

type Message struct {
	Phone   string
	Message string
}

func NewHub() *Hub {
	return &Hub{
		Broadcast:  make(chan *Message, 10),
		Register:   make(chan *Client, 5),
		Unregister: make(chan *Client, 5),
		Clients:    make(map[*Client]bool, 200),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.Register:
			logger.Info("registered", client.Phone)
			h.Clients[client] = true
		case client := <-h.Unregister:
			if _, ok := h.Clients[client]; ok {
				logger.Info("unregistered", client.Phone)
				delete(h.Clients, client)
				close(client.Send)
			}
		case message := <-h.Broadcast:
			for client := range h.Clients {
				if client.Phone != message.Phone {
					continue
				}
				select {
				case client.Send <- message:
					logger.Info("send", client.Phone)
				default:
					close(client.Send)
					delete(h.Clients, client)
				}
			}
		}
	}
}
