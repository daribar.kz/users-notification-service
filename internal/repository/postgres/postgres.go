package postgres

import "database/sql"

type Postgres interface {
	Order
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) *Repository {
	return &Repository{db: db}
}
