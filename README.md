# users-notification-service

### перед запуском проекта необходимо скачать файл из проекта docker-local и положить его в корень проекта, также надо поставить docker-compose и желательно иметь unix систему для локальной разработки. 

## стуктура проекта:

```
workdir daribar
./users-notification-service
./docker-compose.yml
```

### следующим шагом надо поднять проект

```
    docker-compose up --build users_notification_service
```

### как только поднимется сервис, для тестирования можно использовать следующие запросы

```
curl --location --request GET 'localhost:8901/s?phone=87479797932&id=1&isOpen=open'
```
где:
- phone - номер юзера
- id - номер заказа
- isOpen - открытый заказ или закрытый


### для подключения к сокету по эндпоинту /ws необходимо в загаловки вставить phone: <номер>
