package server

import (
	cache2 "daribar/users-notification-service/internal/cache"
	"daribar/users-notification-service/internal/handlers"
	"daribar/users-notification-service/internal/server/notification"
	"github.com/nats-io/nats.go"
	"log"
	"net/http"
)

func Listen(hub *notification.Hub, nc *nats.Conn, cache *cache2.Cache) {
	var handler = handlers.NewHandler(hub, cache, nc)

	http.HandleFunc("/ws", handler.WSHandler)
	http.HandleFunc("/s", handler.WSTestHandler)

	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
