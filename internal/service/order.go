package service

import (
	cache2 "daribar/users-notification-service/internal/cache"
	"daribar/users-notification-service/internal/server/notification"
	"daribar/users-notification-service/pkg/domain"
	"daribar/users-notification-service/pkg/logger"
	"fmt"
)

type OrderService struct {
	hub   *notification.Hub
	cache *cache2.Cache

	Orders chan domain.Order
}

// Order на 100.000 юзеров примерно будет 10мб

func NewOrderService(hub *notification.Hub, cache *cache2.Cache) *OrderService {
	var os = &OrderService{
		hub:    hub,
		Orders: make(chan domain.Order, 100),
		cache:  cache,
	}

	go os.Manage(5)

	return os
}

func (o *OrderService) Manage(size int64) {
	jobs := make(chan domain.Order, 1000)
	defer close(jobs)

	for i := int64(1); i <= size; i++ {
		go o.worker(i, jobs) // 100 workers start
	}

	for order := range o.Orders {
		o.cache.OrderCache.CountOrder(order.IsOpen, order.Phone)

		jobs <- order
	}
}

func (o *OrderService) worker(i int64, jobs <-chan domain.Order) {
	logger.Infof("%d worker started", i)

	for j := range jobs {
		var job = j
		if job.IsOpen {
			var msg = &notification.Message{
				Phone: job.Phone,
				Message: fmt.Sprintf("you have %d orders",
					o.cache.OrderCache.GetCountOpenOrders(job.Phone)),
			}

			o.hub.Broadcast <- msg
		}
	}
}
