package listeners

import (
	"context"
	cache2 "daribar/users-notification-service/internal/cache"
	"daribar/users-notification-service/internal/service"
	"daribar/users-notification-service/pkg/domain"
	"daribar/users-notification-service/pkg/logger"
	"encoding/json"
	"github.com/nats-io/nats.go"
)

type Listener struct {
	service *service.Service
	nc      *nats.Conn
	cache   *cache2.Cache
}

func NewListener(service *service.Service, cache *cache2.Cache, nc *nats.Conn) *Listener {
	return &Listener{
		service: service,
		nc:      nc,
		cache:   cache,
	}
}

type ReceivedOrder struct {
	ID int64 `json:"id"`
}

func (l *Listener) ListenOrderClose(ctx context.Context) {
	_, _ = l.nc.Subscribe("order_close", func(message *nats.Msg) {
		var rcvOrder ReceivedOrder
		err := json.Unmarshal(message.Data, &rcvOrder)
		if err != nil {
			logger.Error(err)
			return
		}

		var order = l.cache.OrderCache.GetOrder(rcvOrder.ID)

		l.cache.OrderCache.CountOrder(false, order.Phone)
	})
}

func (l *Listener) ListenOrderOpen(ctx context.Context) {
	_, _ = l.nc.Subscribe("order_open", func(message *nats.Msg) {
		var rcvOrder ReceivedOrder
		err := json.Unmarshal(message.Data, &rcvOrder)
		if err != nil {
			logger.Error(err)
			return
		}

		var order = l.cache.OrderCache.GetOrder(rcvOrder.ID)
		if order.Nil() {
			l.cache.OrderCache.SetOrder(order) // get from redis or postgres
		}

		logger.Info("ok")

		l.service.Order.Orders <- domain.Order{
			ID:        rcvOrder.ID,
			Phone:     order.Phone,
			CreatedAt: order.CreatedAt,
			IsOpen:    order.IsOpen,
		}
	})
}
