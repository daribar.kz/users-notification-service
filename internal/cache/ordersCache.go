package cache

import (
	"daribar/users-notification-service/pkg/domain"
	"sync"
)

type OrderCache struct {
	ordersCount map[string]int64 // ordersCount[phone]count
	orders      map[int64]domain.Order

	mu sync.RWMutex
}

func NewOrderCache() *OrderCache {
	return &OrderCache{
		ordersCount: make(map[string]int64, 100),
		orders:      make(map[int64]domain.Order, 100),
	}
}

func (o *OrderCache) GetOrder(id int64) domain.Order {
	o.mu.RLock()
	defer o.mu.RUnlock()
	return o.orders[id]
}

func (o *OrderCache) SetOrder(order domain.Order) {
	o.mu.Lock()
	defer o.mu.Unlock()
	o.orders[order.ID] = order
}

func (o *OrderCache) GetCountOpenOrders(phone string) int64 {
	o.mu.RLock()
	defer o.mu.RUnlock()
	return o.ordersCount[phone]
}

func (o *OrderCache) CountOrder(isOpen bool, phone string) {
	o.mu.Lock()
	defer o.mu.Unlock()
	if _, ok := o.ordersCount[phone]; ok {
		if isOpen {
			o.ordersCount[phone]++
			return
		}

		o.ordersCount[phone]--
		return
	}

	o.ordersCount[phone] = 1
}
