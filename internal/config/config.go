package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	PostgresHost     string `envconfig:"POSTGRES_HOST"`
	PostgresDB       string `envconfig:"POSTGRES_DB"`
	PostgresUser     string `envconfig:"POSTGRES_USER"`
	PostgresPort     string `envconfig:"POSTGRES_PORT"`
	PostgresPassword string `envconfig:"POSTGRES_PASSWORD"`
	NatsAddress      string `envconfig:"NATS_ADDRESS"`
	RedisHost        string `envconfig:"REDIS_HOST"`
	RedisUser        string `envconfig:"REDIS_USER"`
	RedisPassword    string `envconfig:"REDIS_PASSWORD"`
}

func New() (*Config, error) {
	var cfg Config
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, err
	}

	return &cfg, nil
}
