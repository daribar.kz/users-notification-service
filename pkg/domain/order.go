package domain

import "time"

type Order struct {
	ID        int64
	Phone     string
	CreatedAt time.Time
	IsOpen    bool
}

func (o *Order) Nil() bool {
	return o.ID == 0
}
